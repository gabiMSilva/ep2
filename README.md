﻿**info:** O programa foi criado através da plataforma NetBeans

# Instruções

  Ao iniciar  programa, você terá duas opções para simular: Distorção Harmônica ou Fluxo de Potência Fundamental. Para começar a simulação desejada basta clicar em uma delas
 *Observação:* Você pode sair do programa a qualquer momento apertando o "x" na parte de cima da janela do mesmo

## Fluxo De Potência Fundamental

  Ao escolher a opção de Simular o Fluxo de Potência Fundamental, abrirá uma janela para que você insira as informações de cada onda. São elas:

 **Onda da Tensão**
 São referentes a ela as duas primeiras caixas de inserção de texto. Nelas você colocará o _Ângulo de Fase da Tensão_ e sua _amplitude_

 **Onda da Corrente**
 A iserção de dados para gerar a onda da corrente acorre de forma similar à da tensão, com a diferença que estes serão colocados na terceira e quarta caixa de texto

 Para voltar para o menu clique no botão "..." ou "<" que está na parte superior esquerda da janela

## Distorção Harmônica

 Ao escolher a opção de simular distorção harmônica, você verá, inicialmente, três caixas de inserção de texto com os respectivos nomes de cada dado que deverão ser inserido naquela caixa. Para gerar as Formas de Onda Harmônica, insira a _amplitude_ e o _ângulo de fase_ da Forma De Onda Fundamental e o número de Ordens Harmônicas que você deseja

 Para voltar ao menu, o processo é o mesmo da primeira simulação
 _Observação:_ Ao inserir a quantidade de ordens harmônicas, apreceram as caixas de texto para que você insira as informações de cada onda. Ao fazer isso e apertar enter, aparecerão os gráficos relacionados a cada uma, porém , ao abaixar a tela dos mesmos através da barra de rolagem esses gráficos somem. Para vê-los novamente, clique em "enviar" com os mesmo dados do gráfico anterior e eles reaparecerão;


# Pontos dos critérios de aceitação

 Nesse programa foram implementados os caso de uso 1, 2 e 3, especificados no _Documento de Requisitos_.
 Dos requisitos do UC2, temos a implementação da Forma de Onda da Tensão, da Forma de Onda da Corrente, da Forma de Onda da Potência Instantânea e do Triângulo de Potências.
 Dos requisitos do UC3, foram implementados a Forma de Onda do Fluxo Fundamental, as Formas de Onda Harmônica e a Forma de Onda Distorcida Resultante.
 Ou seja, todos os requisitos obrigatórios foram implementados. 
