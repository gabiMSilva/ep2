package model;

/**
 *
 * @author gabriela
 */
public class FormaDeOndaFundamental extends DistorcaoHarmonica {
    Double anguloDeFaseFundamentalGraus;
    Double anguloDeFaseFundamental;
    Double amplitudeTensaoFundamental;

    public FormaDeOndaFundamental() {
        this.anguloDeFaseFundamental = 0.0;
        this.amplitudeTensaoFundamental = 0.0;
        this.anguloDeFaseFundamentalGraus = 0.0;
    }

    public Double getAnguloDeFaseFundamental() {
        return anguloDeFaseFundamental;
    }

    public void setAnguloDeFaseFundamental(Double anguloDeFaseFundamental) {
        anguloDeFaseFundamentalGraus = anguloDeFaseFundamental;
        this.anguloDeFaseFundamental = Math.toRadians(anguloDeFaseFundamental);
    }

    public Double getAnguloDeFaseFundamentalGraus() {
        return anguloDeFaseFundamentalGraus;
    }
    
    public Double getAmplitudeTensaoFundamental() {
        return amplitudeTensaoFundamental;
    }

    public void setAmplitudeTensaoFundamental(Double tensaoFundamental) {
        this.amplitudeTensaoFundamental = tensaoFundamental;
    }
    
    //retorna apenas um valor da forma de onda dado um ponto x
    public Double retornarValorY(Double x){
        return amplitudeTensaoFundamental*Math.cos(frequenciaAngular*x + anguloDeFaseFundamental);
    }
    
    @Override
    public void preencherEixoY(){
        for (Double x=0.0; x<(qtdPontosX*step); x= (x+step)){
            scores.add(amplitudeTensaoFundamental*Math.cos(frequenciaAngular*x + anguloDeFaseFundamental));
        }        
    }
}