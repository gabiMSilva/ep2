package model;

import controller.ControllerDistorcaoHarmonica;

/**
 *
 * @author gabriela
 */
public class FormaDeOndaDistorcidaResultante extends DistorcaoHarmonica{
    FormaDeOndaFundamental f;
    FormaDeOndaOrdemHarmonica h;
    Integer numeroHarmonicos;
    private static String serieDeFourier;

    public FormaDeOndaDistorcidaResultante() {
        this.numeroHarmonicos = 0;
        FormaDeOndaDistorcidaResultante.serieDeFourier = "";
    }

    public String getSerieDeFourier() {
        return serieDeFourier;
    }

    public Integer getNumeroHarmonicos() {
        return numeroHarmonicos;
    }

    public void setNumeroHarmonicos(Integer numeroHarmonicos) {
        this.numeroHarmonicos = numeroHarmonicos;
    }
    
    public void setFormaDeOndaFundamental(FormaDeOndaFundamental fundamental){
        f = fundamental;
    }
        
    @Override
    public void preencherEixoY(){
        for (Double x=0.0; x<(qtdPontosX*step); x= (x+step)){
            scores.add(f.retornarValorY(x) + ControllerDistorcaoHarmonica.getSomatorioAmplitude());
        }    
    }
    
    public void calcularSerieFourierAmplitude(){
        serieDeFourier = f.getAmplitudeTensaoFundamental().toString() + "cos(" + f.getAnguloDeFaseFundamentalGraus().toString() + "+" + "ωt)";
    }
    
    public static void calcularSerieDeFourierParteHarmonica(FormaDeOndaOrdemHarmonica h){
        serieDeFourier +=  "+" + h.getAmplitudeTensaoHarmonica().toString() + "cos(" + h.getOrdemHarmonica() + "ωt+" + h.getAnguloDeFaseHarmonicaGraus().toString() + ")"; 
    } 
}
