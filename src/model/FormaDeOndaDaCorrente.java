package model;
/**
 *
 * @author gabriela
 */
public class FormaDeOndaDaCorrente extends FluxoDePotenciaFundamental {
    private Double amplitudeCorrente;
    private Double anguloDeFaseCorrente;

    public FormaDeOndaDaCorrente() {
        this.amplitudeCorrente = 0.0;
        this.anguloDeFaseCorrente = 0.0;        
    }

    public FormaDeOndaDaCorrente(Double amplitudeCorrente, Double anguloDeFaseCorrente) {
        this.amplitudeCorrente = amplitudeCorrente;
        this.anguloDeFaseCorrente = anguloDeFaseCorrente;
    }  

    public Double getAmplitudeCorrente() {
        return amplitudeCorrente;
    }

    public void setAmplitudeCorrente(Double corrente) {
        this.amplitudeCorrente = corrente;
    }

    public Double getAnguloDeFaseCorrente() {
        return anguloDeFaseCorrente;
    }

    public void setAnguloDeFaseCorrente(Double anguloDeFaseCorrente) {
        
        this.anguloDeFaseCorrente = Math.toRadians(anguloDeFaseCorrente);
    }
    //retorna apenas um valor da forma de onda dado um ponto x
    public Double getValorEixoDasOrdenadas(Double valor){
        Double y;
         y = amplitudeCorrente * Math.cos(frequenciaAngular*valor + anguloDeFaseCorrente);
        return y;
    }  
    
    @Override
    public void preencherEixoY(){
        for (Double x = 0.0; x <= (qtdPontosX*step); x = (x + step)) {
            this.scores.add(amplitudeCorrente * Math.cos(frequenciaAngular*x + anguloDeFaseCorrente));
        }
    }
}
