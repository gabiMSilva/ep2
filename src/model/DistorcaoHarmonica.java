/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriela
 */
public abstract class DistorcaoHarmonica{
    public final Double frequenciaAngular = 2*Math.PI*60;
    protected int qtdPontosX;
    protected Double step;
    protected List<Double> scores;

    public DistorcaoHarmonica() {
        this.scores = new ArrayList<>();
    }
    /*Calcula cada um dos pontos do eixo y a partir da função de sua respectiva forma de onda e 
     *  "envia" para o atributo scores
     */
    public void preencherEixoY(){
    }

    public int getQtdPontosX() {
        return qtdPontosX;
    }

    public void setQtdPontosX(int qtdPontosX) {
        this.qtdPontosX = qtdPontosX;
    }

    public Double getStep() {
        return step;
    }

    public void setStep(Double step) {
        this.step = step;
    }

    public List<Double> getScores() {
        return scores;
    }

    public void setScores(List<Double> scores) {
        this.scores = scores;
    }    
}
