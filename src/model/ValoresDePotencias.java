package model;

/**
 *
 * @author gabriela
 */
public class ValoresDePotencias {
    
    private Double valorPotenciaAtiva;
    private Double valorPotenciaReativa;
    private Double valorPotenciaAparente;
    private Double fatorDePotencia;

    public ValoresDePotencias() {
        this.valorPotenciaAtiva = 0.0;
        this.valorPotenciaReativa = 0.0;
        this.valorPotenciaAparente = 0.0;
        this.fatorDePotencia = 0.0;
    }

    public ValoresDePotencias(Double valorPotenciaAtiva, Double valorPotenciaReativa, Double valorPotenciaAparente, Double fatorDePotencia) {
        this.valorPotenciaAtiva = valorPotenciaAtiva;
        this.valorPotenciaReativa = valorPotenciaReativa;
        this.valorPotenciaAparente = valorPotenciaAparente;
        this.fatorDePotencia = fatorDePotencia;
    }

    public Double getValorPotenciaAtiva() {
        return valorPotenciaAtiva;
    }

    public Double getValorPotenciaReativa() {
        return valorPotenciaReativa;
    }

    public Double getValorPotenciaAparente() {
        return valorPotenciaAparente;
    }

    public Double getFatorDePotencia() {
        return fatorDePotencia;
    }
    // Agregação
    public void calcularPotenciaAtiva(FormaDeOndaDaCorrente formaDeOndaC, FormaDeOndaDaTensao formaDeOndaT) {
        this.valorPotenciaAtiva = formaDeOndaT.getAmplitudeTensao()*formaDeOndaC.getAmplitudeCorrente()*Math.cos(formaDeOndaT.getAnguloDeFaseTensao()-formaDeOndaC.getAnguloDeFaseCorrente());
    }
    
    public void calcularPotenciaReativa(FormaDeOndaDaCorrente formaDeOndaC, FormaDeOndaDaTensao formaDeOndaT) {
        this.valorPotenciaReativa = formaDeOndaT.getAmplitudeTensao()*formaDeOndaC.getAmplitudeCorrente()*Math.sin(formaDeOndaT.getAnguloDeFaseTensao()-formaDeOndaC.getAnguloDeFaseCorrente());
    }

    public void calcularPotenciaAparente(FormaDeOndaDaCorrente formaDeOndaC, FormaDeOndaDaTensao formaDeOndaT) {
        this.valorPotenciaAparente = formaDeOndaT.getAmplitudeTensao()*formaDeOndaC.getAmplitudeCorrente();
    }

    public void calcularFatorDePotencia(FormaDeOndaDaCorrente formaDeOndaC, FormaDeOndaDaTensao formaDeOndaT) {
        this.fatorDePotencia = Math.cos(formaDeOndaT.getAnguloDeFaseTensao()-formaDeOndaC.getAnguloDeFaseCorrente());
    }
    
}
