package model;

/**
 *
 * @author gabriela
 */
public class FormaDeOndaOrdemHarmonica extends DistorcaoHarmonica{
    Double anguloDeFaseHarmonica;
    Double anguloDeFaseHarmonicaGraus;
    Double amplitudeTensaoHarmonica;
    Integer ordemHarmonica;

    public FormaDeOndaOrdemHarmonica() {
        this.anguloDeFaseHarmonica = 0.0;
        this.amplitudeTensaoHarmonica = 0.0;
        this.ordemHarmonica = 0;
    }

    public Double getAnguloDeFaseHarmonica() {
        return anguloDeFaseHarmonica;
    }

    public void setAnguloDeFaseHarmonica(Double anguloDeFaseHarmonica) {
        anguloDeFaseHarmonicaGraus = anguloDeFaseHarmonica;
        this.anguloDeFaseHarmonica = Math.toRadians(anguloDeFaseHarmonica);
    }

    public Double getAnguloDeFaseHarmonicaGraus() {
        return anguloDeFaseHarmonicaGraus;
    }
    
    public Double getAmplitudeTensaoHarmonica() {
        return amplitudeTensaoHarmonica;
    }

    public void setAmplitudeTensaoHarmonica(Double tensaoHarmonica) {
        this.amplitudeTensaoHarmonica = tensaoHarmonica;
    }

    public Integer getOrdemHarmonica() {
        return ordemHarmonica;
    }

    public void setOrdemHarmonica(Integer ordemHarmonica) {
        this.ordemHarmonica = ordemHarmonica;
    }
    
    //retorna apenas um valor da forma de onda dado um ponto x
    public Double retornaValorY(Double x){
        return amplitudeTensaoHarmonica*Math.cos(frequenciaAngular*x + anguloDeFaseHarmonica);
    }

    @Override
    public void preencherEixoY(){
        for (Double x=0.0; x<(qtdPontosX*step); x= (x+step)){
            scores.add(amplitudeTensaoHarmonica*Math.cos(frequenciaAngular*x + anguloDeFaseHarmonica));
        }
    }
    
}
