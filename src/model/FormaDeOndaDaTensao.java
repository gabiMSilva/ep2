package model;
/**
 *
 * @author gabriela
 */
public class FormaDeOndaDaTensao extends FluxoDePotenciaFundamental {
    private Double anguloDeFaseTensao;
    private Double amplitudeTensao;    

    public FormaDeOndaDaTensao() {
        this.anguloDeFaseTensao = 0.0;
        this.amplitudeTensao = 0.0;
    }

    public Double getAnguloDeFaseTensao() {
        return anguloDeFaseTensao;
    }

    public void setAnguloDeFaseTensao(Double anguloDeFaseTensao) {
        this.anguloDeFaseTensao = Math.toRadians(anguloDeFaseTensao);
    }

    public Double getAmplitudeTensao() {
        return amplitudeTensao;
    }

    public void setAmplitudeTensao(Double amplitudeTensao) {
        this.amplitudeTensao = amplitudeTensao;
    }
    
    //retorna apenas um valor do eixo y dado um x;
    public Double getValorEixoDasOrdenadas(Double x){
        Double y;
        y = amplitudeTensao * Math.cos(frequenciaAngular*x + anguloDeFaseTensao);        
        return y;
    }
    
    public void preencherEixoY(){
        for (Double x = 0.0; x <= (qtdPontosX*step); x = (x + step)) {
            scores.add(amplitudeTensao * Math.cos(frequenciaAngular*x + anguloDeFaseTensao));
        }
    } 
}