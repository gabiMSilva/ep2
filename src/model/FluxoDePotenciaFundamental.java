package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabriela
 */
public abstract class FluxoDePotenciaFundamental{
    public final Double frequenciaAngular = 2*Math.PI*60;
    protected int qtdPontosX;
    protected Double step;
    protected List<Double> scores;


    public FluxoDePotenciaFundamental() {
        this.scores = new ArrayList<>();
        this.qtdPontosX = 0;
        this.step = 0.0;
    }
    
    public List<Double> getScores() {
        return scores;
    }    

    public int getQtdPontosX() {
        return qtdPontosX;
    }

    public void setQtdPontosX(int qtdPontosX) {
        this.qtdPontosX = qtdPontosX;
    }

    public Double getStep() {
        return step;
    }

    public void setStep(Double step) {
        this.step = step;
    }    
    /*Calcula cada um dos pontos do eixo y a partir da função de sua respectiva forma de onda e 
     *  "envia" para o atributo scores
     */
    public void preencherEixoY(){
    }
}