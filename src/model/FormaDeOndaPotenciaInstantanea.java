/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
/**
 *
 * @author gabriela
 */
public class FormaDeOndaPotenciaInstantanea extends FluxoDePotenciaFundamental{
    
    private FormaDeOndaDaCorrente i;
    private FormaDeOndaDaTensao v;

    public FormaDeOndaPotenciaInstantanea() {
    }

    @Override
    public void preencherEixoY(){
        for (Double x = 0.0; x < (qtdPontosX*step); x = (x + step)) {
            scores.add(i.getValorEixoDasOrdenadas(x)*v.getValorEixoDasOrdenadas(x));
        }
    }   
    /*Agregação
     * Insere a forma de onda da corrente e da tensão nos atributos i e v para 
     * não perder as informações já inseridas nos objetos anteriormente
     */
    public void obterInformacoes(FormaDeOndaDaCorrente corrente, FormaDeOndaDaTensao tensao){
        i = corrente;
        v = tensao;
    }
}