package view;

import controller.ControllerDistorcaoHarmonica;
import controller.ControllerPotenciaFundamental;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.List;
import javax.swing.JFrame;

public class InterfacePrograma extends JFrame {

    private ControllerPotenciaFundamental potenciaFundamentalController;
    private static ControllerDistorcaoHarmonica distorcaoHarmonicaController;
    private GraphPanel graphPanelCorrente;
    private GraphPanel graphPanelTensao;
    private GraphPanel graphPanelPotencias;   private GraphPanel graphPanelFundamental;
    
    public InterfacePrograma() {
        initComponents();
        visibilidadePanels(true, false, false); 
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanelOpcoesDeSimulacao = new javax.swing.JPanel();
        jButtonFluxoDePotenciaFundamental = new javax.swing.JButton();
        jButtonDistorçãoHarmonica = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanelFormaDeOndaDePotenciaInstantanea = new javax.swing.JPanel();
        jPanelTensao = new javax.swing.JPanel();
        jLabelAnguloDeFaseTensao = new javax.swing.JLabel();
        jSpinnerAnguloDeFaseTensao = new javax.swing.JSpinner();
        jLabelAmplitudeTensao = new javax.swing.JLabel();
        jSpinnerAmplitudeTensao = new javax.swing.JSpinner();
        jLabelTensao = new javax.swing.JLabel();
        jButtonVoltar1 = new javax.swing.JButton();
        jPanelCorrente = new javax.swing.JPanel();
        jButtonEnviarAFCorrente = new javax.swing.JButton();
        jSpinnerAmplitudeCorrente = new javax.swing.JSpinner();
        jLabelAnguloDeFaseCorrente = new javax.swing.JLabel();
        jLabelAmplitudeCorrente = new javax.swing.JLabel();
        jSpinnerAnguloDeFaseCorrente = new javax.swing.JSpinner();
        jLabelCorrente = new javax.swing.JLabel();
        jPanelPotencias = new javax.swing.JPanel();
        jTextFieldPotenciaAparente = new javax.swing.JTextField();
        jLabelPotenciaAparente = new javax.swing.JLabel();
        jLabelPotenciaAtiva = new javax.swing.JLabel();
        jTextFieldPotenciaReativa = new javax.swing.JTextField();
        jLabelPotenciaReativa = new javax.swing.JLabel();
        jLabelFatorDePotencia = new javax.swing.JLabel();
        jTextFieldFatorDePotencia = new javax.swing.JTextField();
        jTextFieldPotenciaAtiva = new javax.swing.JTextField();
        jLabelPotenciaInstantanea = new javax.swing.JLabel();
        jPanelTrianguloDePotencias = new javax.swing.JPanel();
        jPanelDistorcaoHarmonica = new javax.swing.JPanel();
        jPanelFundamental = new javax.swing.JPanel();
        jLabelAmplitudeFundamental = new javax.swing.JLabel();
        jLabelAnguloDeFaseComponenteFundamental = new javax.swing.JLabel();
        jSpinnerAmplitudeComponenteFundamental = new javax.swing.JSpinner();
        jSpinnerAnguloDeFaseComponenteFundamental = new javax.swing.JSpinner();
        jLabel1 = new javax.swing.JLabel();
        jButtonVoltar = new javax.swing.JButton();
        jPanelConfigHarmonicos = new javax.swing.JPanel();
        jComboBoxNumeroHarmonicos = new javax.swing.JComboBox<>();
        jLabelHarmonicos = new javax.swing.JLabel();
        jRadioButtonHarmonicoPar = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        jLabelNumeroOrdemHarminnico = new javax.swing.JLabel();
        jButtonNumOrdemHarmonica = new javax.swing.JButton();
        jScrollPaneHarmonicos = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Opções de Simulação");
        setName("Opções De Simulação"); // NOI18N
        setResizable(false);

        jPanelOpcoesDeSimulacao.setMaximumSize(new java.awt.Dimension(1200, 600));
        jPanelOpcoesDeSimulacao.setMinimumSize(new java.awt.Dimension(1200, 600));
        jPanelOpcoesDeSimulacao.setName(""); // NOI18N
        jPanelOpcoesDeSimulacao.setPreferredSize(new java.awt.Dimension(1200, 600));

        jButtonFluxoDePotenciaFundamental.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jButtonFluxoDePotenciaFundamental.setText("Simular Fluxo de Potência Fundamental ");
        jButtonFluxoDePotenciaFundamental.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(133, 58, 193), java.awt.Color.gray));
        jButtonFluxoDePotenciaFundamental.setPreferredSize(new java.awt.Dimension(1200, 300));
        jButtonFluxoDePotenciaFundamental.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonFluxoDePotenciaFundamentalActionPerformed(evt);
            }
        });

        jButtonDistorçãoHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jButtonDistorçãoHarmonica.setText("Simular Distorção Harmônica");
        jButtonDistorçãoHarmonica.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED, new java.awt.Color(133, 58, 193), java.awt.Color.gray));
        jButtonDistorçãoHarmonica.setPreferredSize(new java.awt.Dimension(1200, 200));
        jButtonDistorçãoHarmonica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonDistorçãoHarmonicaActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("URW Gothic L", 1, 48)); // NOI18N
        jLabel2.setText("MENU");

        javax.swing.GroupLayout jPanelOpcoesDeSimulacaoLayout = new javax.swing.GroupLayout(jPanelOpcoesDeSimulacao);
        jPanelOpcoesDeSimulacao.setLayout(jPanelOpcoesDeSimulacaoLayout);
        jPanelOpcoesDeSimulacaoLayout.setHorizontalGroup(
            jPanelOpcoesDeSimulacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelOpcoesDeSimulacaoLayout.createSequentialGroup()
                .addContainerGap(418, Short.MAX_VALUE)
                .addGroup(jPanelOpcoesDeSimulacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jButtonFluxoDePotenciaFundamental, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonDistorçãoHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(487, 487, 487))
        );
        jPanelOpcoesDeSimulacaoLayout.setVerticalGroup(
            jPanelOpcoesDeSimulacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelOpcoesDeSimulacaoLayout.createSequentialGroup()
                .addContainerGap(135, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(97, 97, 97)
                .addComponent(jButtonFluxoDePotenciaFundamental, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButtonDistorçãoHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(179, 179, 179))
        );

        jPanelFormaDeOndaDePotenciaInstantanea.setMaximumSize(new java.awt.Dimension(1200, 600));
        jPanelFormaDeOndaDePotenciaInstantanea.setMinimumSize(new java.awt.Dimension(1200, 600));
        jPanelFormaDeOndaDePotenciaInstantanea.setName(""); // NOI18N
        jPanelFormaDeOndaDePotenciaInstantanea.setPreferredSize(new java.awt.Dimension(1200, 600));
        jPanelFormaDeOndaDePotenciaInstantanea.setLayout(new java.awt.GridLayout(3, 1));

        jPanelTensao.setBorder(null);

        jLabelAnguloDeFaseTensao.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAnguloDeFaseTensao.setText("Ângulo de Fase:");

        jSpinnerAnguloDeFaseTensao.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N

        jLabelAmplitudeTensao.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAmplitudeTensao.setText("Amplitude:");

        jSpinnerAmplitudeTensao.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N

        jLabelTensao.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelTensao.setText("Tensão");

        jButtonVoltar1.setText("<");
        jButtonVoltar1.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonVoltar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoltar1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelTensaoLayout = new javax.swing.GroupLayout(jPanelTensao);
        jPanelTensao.setLayout(jPanelTensaoLayout);
        jPanelTensaoLayout.setHorizontalGroup(
            jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTensaoLayout.createSequentialGroup()
                .addComponent(jButtonVoltar1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelTensaoLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelAmplitudeTensao, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelAnguloDeFaseTensao, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(40, 40, 40)
                        .addGroup(jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSpinnerAmplitudeTensao, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinnerAnguloDeFaseTensao, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(jPanelTensaoLayout.createSequentialGroup()
                        .addGap(342, 342, 342)
                        .addComponent(jLabelTensao)
                        .addContainerGap(777, Short.MAX_VALUE))))
        );
        jPanelTensaoLayout.setVerticalGroup(
            jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelTensaoLayout.createSequentialGroup()
                .addGroup(jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonVoltar1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelTensao))
                .addGap(30, 30, 30)
                .addGroup(jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelAnguloDeFaseTensao, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinnerAnguloDeFaseTensao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addGroup(jPanelTensaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jSpinnerAmplitudeTensao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelAmplitudeTensao, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36))
        );

        jPanelFormaDeOndaDePotenciaInstantanea.add(jPanelTensao);

        jPanelCorrente.setBorder(null);

        jButtonEnviarAFCorrente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jButtonEnviarAFCorrente.setText("Enviar");
        jButtonEnviarAFCorrente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnviarAFCorrenteActionPerformed(evt);
            }
        });

        jSpinnerAmplitudeCorrente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N

        jLabelAnguloDeFaseCorrente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAnguloDeFaseCorrente.setText("Ângulo de Fase:");

        jLabelAmplitudeCorrente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAmplitudeCorrente.setText("Amplitude:");

        jSpinnerAnguloDeFaseCorrente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N

        jLabelCorrente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelCorrente.setText("Corrente");

        javax.swing.GroupLayout jPanelCorrenteLayout = new javax.swing.GroupLayout(jPanelCorrente);
        jPanelCorrente.setLayout(jPanelCorrenteLayout);
        jPanelCorrenteLayout.setHorizontalGroup(
            jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCorrenteLayout.createSequentialGroup()
                .addGroup(jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelCorrenteLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonEnviarAFCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelCorrenteLayout.createSequentialGroup()
                        .addGap(366, 366, 366)
                        .addComponent(jLabelCorrente)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 463, Short.MAX_VALUE)
                        .addGroup(jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelAnguloDeFaseCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelAmplitudeCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(43, 43, 43)
                        .addGroup(jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSpinnerAnguloDeFaseCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinnerAmplitudeCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanelCorrenteLayout.setVerticalGroup(
            jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelCorrenteLayout.createSequentialGroup()
                .addGroup(jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelCorrente)
                    .addGroup(jPanelCorrenteLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelAnguloDeFaseCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinnerAnguloDeFaseCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(40, 40, 40)
                .addGroup(jPanelCorrenteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelAmplitudeCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinnerAmplitudeCorrente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                .addComponent(jButtonEnviarAFCorrente)
                .addContainerGap())
        );

        jPanelFormaDeOndaDePotenciaInstantanea.add(jPanelCorrente);

        jPanelPotencias.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(254, 254, 254)));

        jTextFieldPotenciaAparente.setEditable(false);
        jTextFieldPotenciaAparente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jTextFieldPotenciaAparente.setPreferredSize(new java.awt.Dimension(50, 29));

        jLabelPotenciaAparente.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelPotenciaAparente.setText("Potência Aparente");

        jLabelPotenciaAtiva.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelPotenciaAtiva.setText("Potência Ativa");

        jTextFieldPotenciaReativa.setEditable(false);
        jTextFieldPotenciaReativa.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jTextFieldPotenciaReativa.setPreferredSize(new java.awt.Dimension(50, 29));

        jLabelPotenciaReativa.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelPotenciaReativa.setText("Potência Reativa");

        jLabelFatorDePotencia.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelFatorDePotencia.setText("Fator de Potência");

        jTextFieldFatorDePotencia.setEditable(false);
        jTextFieldFatorDePotencia.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jTextFieldFatorDePotencia.setPreferredSize(new java.awt.Dimension(50, 29));

        jTextFieldPotenciaAtiva.setEditable(false);
        jTextFieldPotenciaAtiva.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jTextFieldPotenciaAtiva.setPreferredSize(new java.awt.Dimension(50, 29));

        jLabelPotenciaInstantanea.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelPotenciaInstantanea.setText("Potência Instântanea");

        jPanelTrianguloDePotencias.setToolTipText("");
        jPanelTrianguloDePotencias.setPreferredSize(new java.awt.Dimension(205, 205));

        javax.swing.GroupLayout jPanelTrianguloDePotenciasLayout = new javax.swing.GroupLayout(jPanelTrianguloDePotencias);
        jPanelTrianguloDePotencias.setLayout(jPanelTrianguloDePotenciasLayout);
        jPanelTrianguloDePotenciasLayout.setHorizontalGroup(
            jPanelTrianguloDePotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 210, Short.MAX_VALUE)
        );
        jPanelTrianguloDePotenciasLayout.setVerticalGroup(
            jPanelTrianguloDePotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 210, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanelPotenciasLayout = new javax.swing.GroupLayout(jPanelPotencias);
        jPanelPotencias.setLayout(jPanelPotenciasLayout);
        jPanelPotenciasLayout.setHorizontalGroup(
            jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelPotenciasLayout.createSequentialGroup()
                .addGap(330, 330, 330)
                .addComponent(jLabelPotenciaInstantanea)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 238, Short.MAX_VALUE)
                .addGroup(jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelPotenciaAparente)
                    .addGroup(jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabelPotenciaReativa, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelPotenciaAtiva, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabelFatorDePotencia, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldPotenciaReativa, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPotenciaAtiva, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldPotenciaAparente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldFatorDePotencia, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelTrianguloDePotencias, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanelPotenciasLayout.setVerticalGroup(
            jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPotenciasLayout.createSequentialGroup()
                .addGroup(jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanelPotenciasLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabelPotenciaReativa, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabelPotenciaAparente, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabelFatorDePotencia, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanelPotenciasLayout.createSequentialGroup()
                        .addGroup(jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanelPotenciasLayout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addGroup(jPanelPotenciasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextFieldPotenciaAtiva, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabelPotenciaAtiva, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabelPotenciaInstantanea))
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldPotenciaReativa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jTextFieldPotenciaAparente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jTextFieldFatorDePotencia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(34, 34, 34))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelPotenciasLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanelTrianguloDePotencias, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanelFormaDeOndaDePotenciaInstantanea.add(jPanelPotencias);

        jPanelDistorcaoHarmonica.setMaximumSize(new java.awt.Dimension(1200, 600));
        jPanelDistorcaoHarmonica.setMinimumSize(new java.awt.Dimension(1200, 600));
        jPanelDistorcaoHarmonica.setPreferredSize(new java.awt.Dimension(1200, 600));

        jPanelFundamental.setBorder(null);
        jPanelFundamental.setPreferredSize(new java.awt.Dimension(600, 120));

        jLabelAmplitudeFundamental.setFont(new java.awt.Font("URW Gothic L", 0, 14)); // NOI18N
        jLabelAmplitudeFundamental.setText("Amplitude:");

        jLabelAnguloDeFaseComponenteFundamental.setFont(new java.awt.Font("URW Gothic L", 0, 14)); // NOI18N
        jLabelAnguloDeFaseComponenteFundamental.setText("Ângulo de fase: ");

        jSpinnerAmplitudeComponenteFundamental.setFont(new java.awt.Font("URW Gothic L", 0, 14)); // NOI18N

        jSpinnerAnguloDeFaseComponenteFundamental.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N

        jLabel1.setText("Forma De Onda Fundamental");

        jButtonVoltar.setText("<");
        jButtonVoltar.setPreferredSize(new java.awt.Dimension(30, 30));
        jButtonVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonVoltarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelFundamentalLayout = new javax.swing.GroupLayout(jPanelFundamental);
        jPanelFundamental.setLayout(jPanelFundamentalLayout);
        jPanelFundamentalLayout.setHorizontalGroup(
            jPanelFundamentalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelFundamentalLayout.createSequentialGroup()
                .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(190, 190, 190)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelFundamentalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelAnguloDeFaseComponenteFundamental)
                    .addComponent(jLabelAmplitudeFundamental))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelFundamentalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSpinnerAmplitudeComponenteFundamental, javax.swing.GroupLayout.DEFAULT_SIZE, 65, Short.MAX_VALUE)
                    .addComponent(jSpinnerAnguloDeFaseComponenteFundamental)))
        );
        jPanelFundamentalLayout.setVerticalGroup(
            jPanelFundamentalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelFundamentalLayout.createSequentialGroup()
                .addGroup(jPanelFundamentalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelFundamentalLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanelFundamentalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelAmplitudeFundamental, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinnerAmplitudeComponenteFundamental, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jLabel1)
                    .addComponent(jButtonVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(jPanelFundamentalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jSpinnerAnguloDeFaseComponenteFundamental, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelAnguloDeFaseComponenteFundamental, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        jPanelConfigHarmonicos.setBorder(null);
        jPanelConfigHarmonicos.setPreferredSize(new java.awt.Dimension(800, 130));

        jComboBoxNumeroHarmonicos.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jComboBoxNumeroHarmonicos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6" }));

        jLabelHarmonicos.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelHarmonicos.setText("Harmônicos:");

        buttonGroup1.add(jRadioButtonHarmonicoPar);
        jRadioButtonHarmonicoPar.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jRadioButtonHarmonicoPar.setText("Pares");

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jRadioButton2.setText("Ímpares");

        jLabelNumeroOrdemHarminnico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelNumeroOrdemHarminnico.setText("Ordem harmônica:");

        jButtonNumOrdemHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jButtonNumOrdemHarmonica.setText("Enviar");
        jButtonNumOrdemHarmonica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonNumOrdemHarmonicaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelConfigHarmonicosLayout = new javax.swing.GroupLayout(jPanelConfigHarmonicos);
        jPanelConfigHarmonicos.setLayout(jPanelConfigHarmonicosLayout);
        jPanelConfigHarmonicosLayout.setHorizontalGroup(
            jPanelConfigHarmonicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelConfigHarmonicosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelConfigHarmonicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabelHarmonicos, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButtonHarmonicoPar, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButton2))
                .addGap(32, 32, 32)
                .addGroup(jPanelConfigHarmonicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jComboBoxNumeroHarmonicos, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelNumeroOrdemHarminnico, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonNumOrdemHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanelConfigHarmonicosLayout.setVerticalGroup(
            jPanelConfigHarmonicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelConfigHarmonicosLayout.createSequentialGroup()
                .addGroup(jPanelConfigHarmonicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNumeroOrdemHarminnico, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelHarmonicos, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanelConfigHarmonicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxNumeroHarmonicos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jRadioButtonHarmonicoPar, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelConfigHarmonicosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonNumOrdemHarmonica))
                .addContainerGap())
        );

        jScrollPaneHarmonicos.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(254, 254, 254)));
        jScrollPaneHarmonicos.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        jPanel1.setBorder(null);
        jPanel1.setPreferredSize(new java.awt.Dimension(1000, 1000));
        jPanel1.setLayout(new java.awt.GridLayout(7, 1));
        jScrollPaneHarmonicos.setViewportView(jPanel1);

        javax.swing.GroupLayout jPanelDistorcaoHarmonicaLayout = new javax.swing.GroupLayout(jPanelDistorcaoHarmonica);
        jPanelDistorcaoHarmonica.setLayout(jPanelDistorcaoHarmonicaLayout);
        jPanelDistorcaoHarmonicaLayout.setHorizontalGroup(
            jPanelDistorcaoHarmonicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDistorcaoHarmonicaLayout.createSequentialGroup()
                .addGroup(jPanelDistorcaoHarmonicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanelFundamental, javax.swing.GroupLayout.DEFAULT_SIZE, 1200, Short.MAX_VALUE)
                    .addComponent(jPanelConfigHarmonicos, javax.swing.GroupLayout.DEFAULT_SIZE, 1200, Short.MAX_VALUE)
                    .addComponent(jScrollPaneHarmonicos, javax.swing.GroupLayout.DEFAULT_SIZE, 1200, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanelDistorcaoHarmonicaLayout.setVerticalGroup(
            jPanelDistorcaoHarmonicaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelDistorcaoHarmonicaLayout.createSequentialGroup()
                .addComponent(jPanelFundamental, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanelConfigHarmonicos, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPaneHarmonicos, javax.swing.GroupLayout.PREFERRED_SIZE, 357, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelOpcoesDeSimulacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanelFormaDeOndaDePotenciaInstantanea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanelDistorcaoHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelOpcoesDeSimulacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanelFormaDeOndaDePotenciaInstantanea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanelDistorcaoHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    //   Retorna o objeto distorcaoHarmonicaContrller para todas as classes/obj que
    //forem utilizarem possam editar o mesmo obj.
    public static ControllerDistorcaoHarmonica getDistorcaoHarmonicaController() {
        return distorcaoHarmonicaController;
    }
    
    private void jButtonFluxoDePotenciaFundamentalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonFluxoDePotenciaFundamentalActionPerformed
        visibilidadePanels(false, true, false);
    }//GEN-LAST:event_jButtonFluxoDePotenciaFundamentalActionPerformed

    private void jButtonEnviarAFCorrenteActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jButtonEnviarAFCorrenteActionPerformed
        potenciaFundamentalController = new ControllerPotenciaFundamental();
        
        /*
        *   Esses "ifs" conferem se o usuário inseriu um valor aceitável dentro 
        *dos limites de cada atributo para, só assim,  dar continuidade ao caso de uso principal
        */        
        if (potenciaFundamentalController.acaoButtonEnviarTensao(jSpinnerAmplitudeTensao, jSpinnerAnguloDeFaseTensao)){
            buildGraphTensao(ControllerPotenciaFundamental.getPontosYTensao()); 
        
            if (potenciaFundamentalController.acaoButtonEnviarCorrente(jTextFieldPotenciaAtiva, 
                jTextFieldPotenciaReativa, jTextFieldPotenciaAparente, jTextFieldFatorDePotencia, 
                jSpinnerAmplitudeCorrente, jSpinnerAnguloDeFaseCorrente, jPanelTrianguloDePotencias)){
                
                buildGraphCorrente(ControllerPotenciaFundamental.getPontosYCorrente());
                buildGraphPotencias(ControllerPotenciaFundamental.getPontosYPotencias());
            }
        }
    }//GEN-LAST:event_jButtonEnviarAFCorrenteActionPerformed

    private void jButtonDistorçãoHarmonicaActionPerformed(ActionEvent evt) {//GEN-FIRST:event_jButtonDistorçãoHarmonicaActionPerformed
        visibilidadePanels(false, false, true);
    }//GEN-LAST:event_jButtonDistorçãoHarmonicaActionPerformed

    private void jButtonNumOrdemHarmonicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonNumOrdemHarmonicaActionPerformed
        distorcaoHarmonicaController = new ControllerDistorcaoHarmonica();
        
        //Sempre que usuário apertar o botão NumOrdemHarmonica será criado um 
        //novo jpanel e um novo gráfico com as novas (ou iguais) informações das caixas de texto
        
        jPanel1 = new javax.swing.JPanel();
        jPanel1.setBorder(null);
        jPanel1.setPreferredSize(new Dimension(1000, 1000));
        jPanel1.setLayout(new GridLayout(7, 1));
        jScrollPaneHarmonicos.setViewportView(jPanel1);
        
        if (distorcaoHarmonicaController.acaoButtonEnviarComponenteFundamental(jSpinnerAnguloDeFaseComponenteFundamental, jSpinnerAmplitudeComponenteFundamental)){
            buildGraphFundamental(ControllerDistorcaoHarmonica.getPontosYFundamental());
        
            distorcaoHarmonicaController.acaoButtonEnviarOpcaoNumeroDeOrdens(jComboBoxNumeroHarmonicos);
            configPanelHarmonicos(); 
        }    
    }//GEN-LAST:event_jButtonNumOrdemHarmonicaActionPerformed

    private void jButtonVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltarActionPerformed
        visibilidadePanels(true, false, false);
    }//GEN-LAST:event_jButtonVoltarActionPerformed

    private void jButtonVoltar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonVoltar1ActionPerformed
        visibilidadePanels(true, false, false);
    }//GEN-LAST:event_jButtonVoltar1ActionPerformed
    private void configPanelHarmonicos(){
        
       /*
        Esse fragmento do código será executado n vezes, onde n é o número de 
        ordens harmônicas inseridas pelo usuário. Sendo que, em cada uma dessas 
        vezes será criado uma gráfico(forma de onda) e um jpanel em que esse gráfico 
        será colocado com as informações inseridas pelo usuário (amplitude e ângulo de fase da onda)
        */
        for(int i=1; i<=distorcaoHarmonicaController.getNumeroDeOrdensHarmonicas(); i++){
            if (i == distorcaoHarmonicaController.getNumeroDeOrdensHarmonicas()){
                
                /*
                Esse if foi criado para que quando a última forma de onda for feita
                seja criado um novo jPanel com uma forma de onda resultante e com 
                a série de fourier à mostra.
                */
                
                JPanelFormaDeOndaHarmonicaUltimo jPanelHarmonicoUlt = new JPanelFormaDeOndaHarmonicaUltimo();
                jPanel1.add(jPanelHarmonicoUlt);
                
                JPanelFormaDeOndaResultante resultante = new JPanelFormaDeOndaResultante();
                jPanel1.add(resultante);
                ControllerDistorcaoHarmonica.setjPanelResultante(resultante);
            }
            else {
                JPanelFormaDeOndaHarmonica jPanelHarmonico = new JPanelFormaDeOndaHarmonica();
                jPanel1.add(jPanelHarmonico);      
            }
        }
    }
    /*
      Todos os métodos buildGraph serão responsáveis por criar um gráfico, 
    adicioná-los em um jpanel e editar alguns de seus atributos de acordo com a
    necessidade de cada gráfico específico
    */
    private void buildGraphTensao(List<Double> points) {
        graphPanelTensao = new GraphPanel(points);
        jPanelTensao.add(graphPanelTensao); 
        graphPanelTensao.setBounds(0, 20, 700, 180);       
    }
    
    private void buildGraphCorrente(List<Double> points) {
        graphPanelCorrente = new GraphPanel(points);
        jPanelCorrente.add(graphPanelCorrente);
        graphPanelCorrente.setBounds(0, 20, 700, 180);
    }
    
    private void buildGraphPotencias(List<Double> points) {
        graphPanelPotencias = new GraphPanel(points);
        jPanelPotencias.add(graphPanelPotencias);
        graphPanelPotencias.setBounds(0, 20, 700, 180);
    }
    
    private void buildGraphFundamental(List<Double> points) {
        graphPanelFundamental = new GraphPanel(points);
        jPanelFundamental.add(graphPanelFundamental);
        graphPanelFundamental.setBounds(-10, 0, 550, 150);
    }
    
    /*
       Esse método facilita a edição quanto a qual tela aparecerá em cada momento:
    a tela do menu, que contém as opções de simulação, e a tela de cada uma
    duas simulações apresentadas
    */
    private void visibilidadePanels(boolean opSimulacoes, boolean PotenciaInstantanea, boolean distHarmonica){
        jPanelOpcoesDeSimulacao.setVisible(opSimulacoes);
        jPanelFormaDeOndaDePotenciaInstantanea.setVisible(PotenciaInstantanea);
        jPanelDistorcaoHarmonica.setVisible(distHarmonica);
    }

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfacePrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfacePrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfacePrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfacePrograma.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new InterfacePrograma().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButtonDistorçãoHarmonica;
    private javax.swing.JButton jButtonEnviarAFCorrente;
    private javax.swing.JButton jButtonFluxoDePotenciaFundamental;
    private javax.swing.JButton jButtonNumOrdemHarmonica;
    private javax.swing.JButton jButtonVoltar;
    private javax.swing.JButton jButtonVoltar1;
    private javax.swing.JComboBox<String> jComboBoxNumeroHarmonicos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabelAmplitudeCorrente;
    private javax.swing.JLabel jLabelAmplitudeFundamental;
    private javax.swing.JLabel jLabelAmplitudeTensao;
    private javax.swing.JLabel jLabelAnguloDeFaseComponenteFundamental;
    private javax.swing.JLabel jLabelAnguloDeFaseCorrente;
    private javax.swing.JLabel jLabelAnguloDeFaseTensao;
    private javax.swing.JLabel jLabelCorrente;
    private javax.swing.JLabel jLabelFatorDePotencia;
    private javax.swing.JLabel jLabelHarmonicos;
    private javax.swing.JLabel jLabelNumeroOrdemHarminnico;
    private javax.swing.JLabel jLabelPotenciaAparente;
    private javax.swing.JLabel jLabelPotenciaAtiva;
    private javax.swing.JLabel jLabelPotenciaInstantanea;
    private javax.swing.JLabel jLabelPotenciaReativa;
    private javax.swing.JLabel jLabelTensao;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelConfigHarmonicos;
    private javax.swing.JPanel jPanelCorrente;
    private javax.swing.JPanel jPanelDistorcaoHarmonica;
    private javax.swing.JPanel jPanelFormaDeOndaDePotenciaInstantanea;
    private javax.swing.JPanel jPanelFundamental;
    private javax.swing.JPanel jPanelOpcoesDeSimulacao;
    private javax.swing.JPanel jPanelPotencias;
    private javax.swing.JPanel jPanelTensao;
    private javax.swing.JPanel jPanelTrianguloDePotencias;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JRadioButton jRadioButtonHarmonicoPar;
    private javax.swing.JScrollPane jScrollPaneHarmonicos;
    private javax.swing.JSpinner jSpinnerAmplitudeComponenteFundamental;
    private javax.swing.JSpinner jSpinnerAmplitudeCorrente;
    private javax.swing.JSpinner jSpinnerAmplitudeTensao;
    private javax.swing.JSpinner jSpinnerAnguloDeFaseComponenteFundamental;
    private javax.swing.JSpinner jSpinnerAnguloDeFaseCorrente;
    private javax.swing.JSpinner jSpinnerAnguloDeFaseTensao;
    private javax.swing.JTextField jTextFieldFatorDePotencia;
    private javax.swing.JTextField jTextFieldPotenciaAparente;
    private javax.swing.JTextField jTextFieldPotenciaAtiva;
    private javax.swing.JTextField jTextFieldPotenciaReativa;
    // End of variables declaration//GEN-END:variables
}
