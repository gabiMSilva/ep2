package view;

import controller.ControllerDistorcaoHarmonica;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author gabriela
 */
public class JPanelFormaDeOndaHarmonicaUltimo extends JPanel {
    ControllerDistorcaoHarmonica distHarmonica;
    GraphPanel graphPanelFormaDeOndaHarmonica;
    GraphPanel graphPanelResultante;
    static JPanel panelResultante;

    public JPanelFormaDeOndaHarmonicaUltimo() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelAnguloDeFasePanelHarmonico = new javax.swing.JLabel();
        jLabelOrdemHarmonica = new javax.swing.JLabel();
        jLabelAmplitudePanelHarmonico = new javax.swing.JLabel();
        jSpinnerAnguloHarmonico = new javax.swing.JSpinner();
        jSpinnerAmplitudeHarmonica = new javax.swing.JSpinner();
        jButtonEnviarPanelHarmonico = new javax.swing.JButton();
        jSpinnerOrdemHarmonica = new javax.swing.JSpinner();
        jPanelGraphHarmonico = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(254, 254, 254)));

        jLabelAnguloDeFasePanelHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAnguloDeFasePanelHarmonico.setText("Ângulo de fase:");
        jLabelAnguloDeFasePanelHarmonico.setPreferredSize(new java.awt.Dimension(130, 25));

        jLabelOrdemHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelOrdemHarmonica.setText("Ordem harmônica:");
        jLabelOrdemHarmonica.setPreferredSize(new java.awt.Dimension(130, 25));

        jLabelAmplitudePanelHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAmplitudePanelHarmonico.setText("Amplitude:");
        jLabelAmplitudePanelHarmonico.setPreferredSize(new java.awt.Dimension(130, 25));

        jSpinnerAnguloHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jSpinnerAnguloHarmonico.setMinimumSize(new java.awt.Dimension(0, 0));
        jSpinnerAnguloHarmonico.setPreferredSize(new java.awt.Dimension(40, 20));

        jSpinnerAmplitudeHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jSpinnerAmplitudeHarmonica.setMinimumSize(new java.awt.Dimension(0, 0));
        jSpinnerAmplitudeHarmonica.setPreferredSize(new java.awt.Dimension(40, 20));

        jButtonEnviarPanelHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jButtonEnviarPanelHarmonico.setText("Enviar");
        jButtonEnviarPanelHarmonico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnviarPanelHarmonicoActionPerformed(evt);
            }
        });

        jSpinnerOrdemHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jSpinnerOrdemHarmonica.setMinimumSize(new java.awt.Dimension(0, 0));
        jSpinnerOrdemHarmonica.setPreferredSize(new java.awt.Dimension(40, 20));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelGraphHarmonico, javax.swing.GroupLayout.DEFAULT_SIZE, 936, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(jButtonEnviarPanelHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelAmplitudePanelHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelAnguloDeFasePanelHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabelOrdemHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSpinnerAmplitudeHarmonica, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinnerAnguloHarmonico, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinnerOrdemHarmonica, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelAmplitudePanelHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jSpinnerAmplitudeHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelAnguloDeFasePanelHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jSpinnerAnguloHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelOrdemHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jSpinnerOrdemHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonEnviarPanelHarmonico)
                        .addGap(38, 38, 38))
                    .addComponent(jPanelGraphHarmonico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    public static void setPanelResultante(JPanel panelResultante) {
        JPanelFormaDeOndaHarmonicaUltimo.panelResultante = panelResultante;
    }
    
    private void jButtonEnviarPanelHarmonicoActionPerformed(java.awt.event.ActionEvent evt) {                                                            
        distHarmonica = InterfacePrograma.getDistorcaoHarmonicaController();
        
        if (distHarmonica.acaoButtonEnviarPanelDistHarmonicaUltimo(jSpinnerAmplitudeHarmonica, jSpinnerAnguloHarmonico, jSpinnerOrdemHarmonica, jPanelGraphHarmonico)){
            //cria gráfico harmônico
            buildGraphHarmonico(distHarmonica.getPontosYHarmonica());
            
            /* chama o método configFormaDeOndaResultante para calcular os 
            valores do eixo y e a série de fourier
            */
            distHarmonica.configFormaDeOndaResultante();  
            /*
            Chama o método getjPanelResultante para que seja editado o mesmo objeto
            que o usado na classe que ele foi instanciado
            */
            JPanelFormaDeOndaResultante resultante = distHarmonica.getjPanelResultante();        
            buildGraphResultante(distHarmonica.getPontosYHarmonica(), resultante.getjPanelGraphResultante());
            distHarmonica.configPanelResultante();
        }
    }
    private void buildGraphHarmonico(List<Double> points) {
        graphPanelFormaDeOndaHarmonica = new GraphPanel(points);
        jPanelGraphHarmonico.add(graphPanelFormaDeOndaHarmonica); 
        graphPanelFormaDeOndaHarmonica.setBounds(-10, -20, 550, 150);
    }
    
    private void buildGraphResultante(List<Double> points, JPanel jPanelResultante) {
        graphPanelResultante = new GraphPanel(points);
        jPanelResultante.add(graphPanelResultante); 
        graphPanelResultante.setBounds(-10, -20, 550, 150);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonEnviarPanelHarmonico;
    private javax.swing.JLabel jLabelAmplitudePanelHarmonico;
    private javax.swing.JLabel jLabelAnguloDeFasePanelHarmonico;
    private javax.swing.JLabel jLabelOrdemHarmonica;
    private javax.swing.JPanel jPanelGraphHarmonico;
    private javax.swing.JSpinner jSpinnerAmplitudeHarmonica;
    private javax.swing.JSpinner jSpinnerAnguloHarmonico;
    private javax.swing.JSpinner jSpinnerOrdemHarmonica;
    // End of variables declaration//GEN-END:variables
}
