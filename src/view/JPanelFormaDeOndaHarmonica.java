package view;

import controller.ControllerDistorcaoHarmonica;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author gabriela
 */
public class JPanelFormaDeOndaHarmonica extends JPanel {
    ControllerDistorcaoHarmonica distHarmonica;
    GraphPanel graphPanelFormaDeOndaHarmonica;

    public JPanelFormaDeOndaHarmonica() {
        initComponents();
        distHarmonica = InterfacePrograma.getDistorcaoHarmonicaController();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelAnguloDeFasePanelHarmonico = new javax.swing.JLabel();
        jSpinnerAnguloHarmonico = new javax.swing.JSpinner();
        jSpinnerAmplitudeHarmonica = new javax.swing.JSpinner();
        jLabelAmplitudePanelHarmonico = new javax.swing.JLabel();
        jButtonEnviarPanelHarmonico = new javax.swing.JButton();
        jLabelOrdemHarmonica = new javax.swing.JLabel();
        jSpinnerOrdemHarmonica = new javax.swing.JSpinner();
        jPanelGraphHarmonico = new javax.swing.JPanel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(254, 254, 254)));

        jLabelAnguloDeFasePanelHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAnguloDeFasePanelHarmonico.setText("Ângulo de fase:");

        jSpinnerAnguloHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jSpinnerAnguloHarmonico.setPreferredSize(new java.awt.Dimension(39, 20));

        jSpinnerAmplitudeHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jSpinnerAmplitudeHarmonica.setPreferredSize(new java.awt.Dimension(39, 20));

        jLabelAmplitudePanelHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelAmplitudePanelHarmonico.setText("Amplitude:");

        jButtonEnviarPanelHarmonico.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jButtonEnviarPanelHarmonico.setText("Enviar");
        jButtonEnviarPanelHarmonico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonEnviarPanelHarmonicoActionPerformed(evt);
            }
        });

        jLabelOrdemHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelOrdemHarmonica.setText("Ordem harmônica:");

        jSpinnerOrdemHarmonica.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jSpinnerOrdemHarmonica.setPreferredSize(new java.awt.Dimension(39, 20));

        jPanelGraphHarmonico.setPreferredSize(new java.awt.Dimension(370, 140));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelGraphHarmonico, javax.swing.GroupLayout.DEFAULT_SIZE, 933, Short.MAX_VALUE)
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelOrdemHarmonica, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelAnguloDeFasePanelHarmonico, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelAmplitudePanelHarmonico, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonEnviarPanelHarmonico, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinnerOrdemHarmonica, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinnerAnguloHarmonico, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSpinnerAmplitudeHarmonica, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelGraphHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelAmplitudePanelHarmonico)
                            .addComponent(jSpinnerAmplitudeHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelAnguloDeFasePanelHarmonico)
                            .addComponent(jSpinnerAnguloHarmonico, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabelOrdemHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jSpinnerOrdemHarmonica, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonEnviarPanelHarmonico)))
                .addGap(0, 55, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonEnviarPanelHarmonicoActionPerformed(java.awt.event.ActionEvent evt) {
        /*
        *   Esse "if" confere se o usuário inseriu um valor aceitável dentro 
        *dos limites de cada atributo para, só assim,  dar continuidade ao caso de uso principal
        */
        if(distHarmonica.acaoButtonEnviarPanelDistHarmonica(jSpinnerAmplitudeHarmonica, jSpinnerAnguloHarmonico, jSpinnerOrdemHarmonica, jPanelGraphHarmonico)){
            buildGraphTensao(distHarmonica.getPontosYHarmonica());
        }
    }

    private void buildGraphTensao(List<Double> points) {
        graphPanelFormaDeOndaHarmonica = new GraphPanel(points);
        jPanelGraphHarmonico.add(graphPanelFormaDeOndaHarmonica); 
        graphPanelFormaDeOndaHarmonica.setBounds(-10, -0, 550, 150);
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonEnviarPanelHarmonico;
    private javax.swing.JLabel jLabelAmplitudePanelHarmonico;
    private javax.swing.JLabel jLabelAnguloDeFasePanelHarmonico;
    private javax.swing.JLabel jLabelOrdemHarmonica;
    private javax.swing.JPanel jPanelGraphHarmonico;
    private javax.swing.JSpinner jSpinnerAmplitudeHarmonica;
    private javax.swing.JSpinner jSpinnerAnguloHarmonico;
    private javax.swing.JSpinner jSpinnerOrdemHarmonica;
    // End of variables declaration//GEN-END:variables
}
