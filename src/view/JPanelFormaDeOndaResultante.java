/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author gabriela
 */
public class JPanelFormaDeOndaResultante extends JPanel {
   
    public JPanelFormaDeOndaResultante() {
        initComponents();
    }

    public JPanel getjPanelGraphResultante() {
        return jPanelGraphResultante;
    }

    public JTextField getjTextFieldSerieFourierResultante() {
        return jTextFieldSerieFourierResultante;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelSerieDeFourier = new javax.swing.JLabel();
        jPanelGraphResultante = new javax.swing.JPanel();
        jTextFieldSerieFourierResultante = new javax.swing.JTextField();

        jLabelSerieDeFourier.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jLabelSerieDeFourier.setText("Série de Fourier Amplitude-Fase");

        javax.swing.GroupLayout jPanelGraphResultanteLayout = new javax.swing.GroupLayout(jPanelGraphResultante);
        jPanelGraphResultante.setLayout(jPanelGraphResultanteLayout);
        jPanelGraphResultanteLayout.setHorizontalGroup(
            jPanelGraphResultanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 651, Short.MAX_VALUE)
        );
        jPanelGraphResultanteLayout.setVerticalGroup(
            jPanelGraphResultanteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jTextFieldSerieFourierResultante.setEditable(false);
        jTextFieldSerieFourierResultante.setFont(new java.awt.Font("URW Gothic L", 0, 15)); // NOI18N
        jTextFieldSerieFourierResultante.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelGraphResultante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(119, 119, 119)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabelSerieDeFourier)
                    .addComponent(jTextFieldSerieFourierResultante, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelSerieDeFourier, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(40, 40, 40)
                        .addComponent(jTextFieldSerieFourierResultante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))
                    .addComponent(jPanelGraphResultante, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabelSerieDeFourier;
    private javax.swing.JPanel jPanelGraphResultante;
    private javax.swing.JTextField jTextFieldSerieFourierResultante;
    // End of variables declaration//GEN-END:variables
}
