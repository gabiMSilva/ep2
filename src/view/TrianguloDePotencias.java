package view;


import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
/**
 *
 * @author gabriela
 */
public class TrianguloDePotencias extends JPanel {
    int angulo;
    int potAtiva;
    int potReativa;

    public TrianguloDePotencias(int altura, int largura, double angulo, double potAtiva, double potReativa) {
        initComponents();
        setSize(largura, altura);
        this.angulo = (int)angulo;
        this.potAtiva = (int) potAtiva/100;
        this.potReativa = (int) potReativa/100;
        setVisible(true);
    }
        
    @Override
    public void paint(Graphics g){
        //Desenha os eixos do plano cartesiano
        super.paintComponent(g);
        g.setColor(Color.black);
        g.drawLine(0, getHeight()/2, getWidth(), getHeight()/2);
        g.drawLine(getWidth()/2, 0, getWidth()/2, getHeight());
        //Desenha o triângulo
        g.setColor(Color.blue); //potência ativa (P)
        g.drawLine(getWidth()/2, getHeight()/2, potAtiva + getWidth()/2, getHeight()/2);
        g.setColor(Color.orange); //potência reativa (Q)
        g.drawLine(potAtiva + getWidth()/2, getHeight()/2, potAtiva + getWidth()/2, getHeight()/2 - potReativa);
        g.setColor(Color.red); //potência aparente (S)
        g.drawLine(getWidth()/2, getHeight()/2, potAtiva + getWidth()/2, getHeight()/2 - potReativa);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents
}
