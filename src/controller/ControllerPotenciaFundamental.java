/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.text.DecimalFormat;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import model.FormaDeOndaDaCorrente;
import model.FormaDeOndaDaTensao;
import model.FormaDeOndaPotenciaInstantanea;
import model.ValoresDePotencias;
import view.TrianguloDePotencias;

/**
 *
 * @author gabriela
 */
public class ControllerPotenciaFundamental {
    FormaDeOndaDaTensao ondaTensao ;
    FormaDeOndaDaCorrente ondaCorrente;
    FormaDeOndaPotenciaInstantanea potencia;
    ValoresDePotencias valores; 
    TrianguloDePotencias triangulo;
    private static List<Double> PontosYPotencias;
    private static List<Double> PontosYCorrente;
    private static List<Double> PontosYTensao;

    public ControllerPotenciaFundamental() {
    }


    public static List<Double> getPontosYPotencias() {
        return PontosYPotencias;
    }

    public static List<Double> getPontosYCorrente() {
        return PontosYCorrente;
    }

    public static List<Double> getPontosYTensao() {
        return PontosYTensao;
    }
    
    public ValoresDePotencias getValores() {
        return valores;
    }

    public void setValores(ValoresDePotencias valores) {
        this.valores = valores;
    }

    public boolean acaoButtonEnviarTensao(JSpinner ampTensao, JSpinner anguloTensao){
        ondaTensao = new FormaDeOndaDaTensao();
      
        if (Double.parseDouble(anguloTensao.getValue().toString()) < -180 || Double.parseDouble(anguloTensao.getValue().toString()) > 180){
            JOptionPane.showMessageDialog(null, "O ângulo de fase deve estar entre -180º e 180º");
            return false;
        }
        if (Double.parseDouble(ampTensao.getValue().toString()) < 0 || Double.parseDouble(ampTensao.getValue().toString()) > 220){
            JOptionPane.showMessageDialog(null, "O valor RMS da amplitude da tensão deve estar entre 0 e 220");
            return false;
        }
        else {
            ondaTensao.setAnguloDeFaseTensao(Double.parseDouble(anguloTensao.getValue().toString()));
            ondaTensao.setAmplitudeTensao(Double.parseDouble(ampTensao.getValue().toString()));
            ondaTensao.setStep(0.001);
            ondaTensao.setQtdPontosX(50);
            ondaTensao.preencherEixoY();
            PontosYTensao = ondaTensao.getScores();
            return true;
        }
    }
       
    public boolean acaoButtonEnviarCorrente(JTextField potenciaAtiva, JTextField potenciaReativa, 
            JTextField potenciaAparente, JTextField fatorDePotencia, JSpinner ampCorrente, 
            JSpinner anguloCorrente, JPanel panelTrianguloDePotencias){
        
        ondaCorrente = new FormaDeOndaDaCorrente();
        DecimalFormat df = new DecimalFormat("0.00");
        
        if (Double.parseDouble(ampCorrente.getValue().toString()) > 100 || Double.parseDouble(ampCorrente.getValue().toString()) < 0){
            JOptionPane.showMessageDialog(null, "O valor RMS da amplitude da corrente deve estar entre 0 e 220");
            return false;
        }
        
        if (Double.parseDouble(anguloCorrente.getValue().toString()) < -180 || Double.parseDouble(anguloCorrente.getValue().toString()) > 180 ){
            JOptionPane.showMessageDialog(null, "Os ângulos de fase devem estar entre -180° e 180°");
            return false;
        }
        
        else {
            ondaCorrente.setAmplitudeCorrente(Double.parseDouble(ampCorrente.getValue().toString()));        
            ondaCorrente.setAnguloDeFaseCorrente(Double.parseDouble(anguloCorrente.getValue().toString()));

            valores = new ValoresDePotencias();      
            potencia = new FormaDeOndaPotenciaInstantanea();
            
            valores.calcularPotenciaAtiva(ondaCorrente, ondaTensao);
            valores.calcularPotenciaReativa(ondaCorrente, ondaTensao);
            valores.calcularFatorDePotencia(ondaCorrente, ondaTensao);
            valores.calcularPotenciaAparente(ondaCorrente, ondaTensao);
            
            Double pAtiva = Double.parseDouble(String.valueOf(valores.getValorPotenciaAtiva()));
            Double pReativa = Double.parseDouble(String.valueOf(valores.getValorPotenciaReativa()));
            Double ftDePotencia = Double.parseDouble(String.valueOf(valores.getFatorDePotencia()));
            Double pAparente = Double.parseDouble(String.valueOf(valores.getValorPotenciaAparente()));

            String potAtiva = df.format(pAtiva);
            String potReativa = df.format(pReativa);
            String potAparente = df.format(pAparente);
            String fatPotencia = df.format(ftDePotencia);

            potenciaAtiva.setText(potAtiva);
            potenciaReativa.setText(potReativa);
            potenciaAparente.setText(potAparente);
            fatorDePotencia.setText(fatPotencia);

            ondaCorrente.setStep(0.001);
            ondaCorrente.setQtdPontosX(50);
            ondaCorrente.preencherEixoY();
            PontosYCorrente = ondaCorrente.getScores();

            potencia.obterInformacoes(ondaCorrente, ondaTensao);
            potencia.setStep(0.0005);
            potencia.setQtdPontosX(60);
            potencia.preencherEixoY();
            PontosYPotencias = potencia.getScores();
            
            triangulo = new TrianguloDePotencias(190, 190, ftDePotencia, pAtiva, pReativa);
            panelTrianguloDePotencias.add(triangulo);
            panelTrianguloDePotencias.repaint();
            panelTrianguloDePotencias.setVisible(true);
            triangulo.setVisible(true);
                     
            return true;
        }
    }
}