package controller;

import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import model.FormaDeOndaDistorcidaResultante;
import model.FormaDeOndaFundamental;
import model.FormaDeOndaOrdemHarmonica;
import view.JPanelFormaDeOndaResultante;

/**
 *
 * @author gabriela
 */
public class ControllerDistorcaoHarmonica {
    private FormaDeOndaFundamental objFundamental;
    private FormaDeOndaDistorcidaResultante objDistorcida;
    private Integer numeroDeOrdensHarmonicas;
    private static JPanelFormaDeOndaResultante jPanelResultante;
    private static Double somatorioAmplitude;
    private static List<Double> pontosYFundamental;
    private static List<Double> pontosYDistorcida;
    private List<Double> pontosYHarmonica;
    

    public ControllerDistorcaoHarmonica() {
        numeroDeOrdensHarmonicas = 1;
        ControllerDistorcaoHarmonica.somatorioAmplitude = 0.0;
    }

    public JPanelFormaDeOndaResultante getjPanelResultante() {
        return jPanelResultante;
    }

    public static void setjPanelResultante(JPanelFormaDeOndaResultante jPanelResultante) {
        ControllerDistorcaoHarmonica.jPanelResultante = jPanelResultante;
    }
 
    public Integer getNumeroDeOrdensHarmonicas() {
        return numeroDeOrdensHarmonicas;
    }

    public void setNumeroDeOrdensHarmonicas(Integer numeroDeOrdensHarmonicas) {
        this.numeroDeOrdensHarmonicas = numeroDeOrdensHarmonicas;
    }

    public static List<Double> getPontosYFundamental() {
        return pontosYFundamental;
    }

    public static List<Double> getPontosYDistorcida() {
        return pontosYDistorcida;
    }

    public List<Double> getPontosYHarmonica() {
        return pontosYHarmonica;
    }

    public void setPontosYHarmonica(List<Double> pontosYHarmonica) {
        this.pontosYHarmonica = pontosYHarmonica;
    }

    public static Double getSomatorioAmplitude() {
        return somatorioAmplitude;
    }
     
    /* Vai ser executado quando usuário clicar no botão "enviar" na opção de  
     *    fluxo de potência Fundamental
    */
    public boolean acaoButtonEnviarComponenteFundamental(JSpinner angFaseCompFundamental, JSpinner amplitudeCompFundamental){
        objFundamental = new FormaDeOndaFundamental();
        
        /*   Define os limites dos valores de amplitude e ângulo de fase.
         *   Caso eles não sejam obedecidos, esse método retornará false e 
         * aprecerá uma janela para o usuário descrevendo quais limites foram
         * ultrapassados e qual o intervalo será aceito.
         *   O componente usado para inserir esses valores(JSpinner) não aceita caracteres
         * que não são números. Fazendo desnecessário o uso de excessão nessa 
         * parte do código com esse objetivo.
        */
        if (Double.parseDouble(angFaseCompFundamental.getValue().toString()) < -180 || Double.parseDouble(angFaseCompFundamental.getValue().toString()) > 180){
            JOptionPane.showMessageDialog(null, "Os ângulos de fase devem estar entre -180° e 180°");

      
            return false;
        }
        if (Double.parseDouble(amplitudeCompFundamental.getValue().toString()) < 0 || Double.parseDouble(amplitudeCompFundamental.getValue().toString()) > 220){
            JOptionPane.showMessageDialog(null, "O valor RMS da amplitude deve estar entre 0 e 220");
            return false;
        }
        
        else{
            objFundamental.setAnguloDeFaseFundamental(Double.parseDouble(angFaseCompFundamental.getValue().toString()));
            objFundamental.setAmplitudeTensaoFundamental(Double.parseDouble(amplitudeCompFundamental.getValue().toString()));

            objFundamental.setStep(0.001);
            objFundamental.setQtdPontosX(40);
            objFundamental.preencherEixoY();

            objDistorcida = new FormaDeOndaDistorcidaResultante();

            pontosYFundamental = objFundamental.getScores(); 
            objDistorcida.setFormaDeOndaFundamental(objFundamental);
            return true;
        }
    }
    
    public void acaoButtonEnviarOpcaoNumeroDeOrdens(JComboBox numeroDeOrdens){
        numeroDeOrdensHarmonicas = Integer.parseInt(numeroDeOrdens.getSelectedItem().toString());
    }
    
    public boolean acaoButtonEnviarPanelDistHarmonica(JSpinner amplitude, JSpinner angulo, JSpinner ordemHarmonica, JPanel panelGraph){
        FormaDeOndaOrdemHarmonica objHarmonica = new FormaDeOndaOrdemHarmonica();
        
        /*   Define os limites dos valores de amplitude e ângulo de fase.
         *   Caso eles não sejam obedecidos, esse método retornará false e 
         * aprecerá uma janela para o usuário descrevendo quais limites foram
         * ultrapassados e qual o intervalo será aceito.
         *   O componente usado para inserir esses valores(JSpinner) não aceita caracteres
         * que não são números. Fazendo desnecessário o uso de excessão nessa 
         * parte do código com esse objetivo.
        */        
        if (Double.parseDouble(angulo.getValue().toString()) < -180 || Double.parseDouble(angulo.getValue().toString()) > 180){
            JOptionPane.showMessageDialog(null, "Os ângulos de fase devem estar entre -180° e 180°");
            return false;
        }
        if (Double.parseDouble(amplitude.getValue().toString()) > 220 || Double.parseDouble(amplitude.getValue().toString()) < 0){
            JOptionPane.showMessageDialog(null, "O valor RMS da amplitude deve estar entre 0 e 220");
            return false;
        }
        if (Integer.parseInt(ordemHarmonica.getValue().toString()) < 0 || Integer.parseInt(ordemHarmonica.getValue().toString()) > 15){
            JOptionPane.showMessageDialog(null, "As ordens harmônicas devem estar entre 0 e 15");
            return false;
        }
        else {
            objHarmonica.setAnguloDeFaseHarmonica(Double.parseDouble(angulo.getValue().toString()));
            objHarmonica.setAmplitudeTensaoHarmonica(Double.parseDouble(amplitude.getValue().toString()));
            objHarmonica.setOrdemHarmonica(Integer.parseInt(ordemHarmonica.getValue().toString()));

            objHarmonica.setStep(0.001);
            objHarmonica.setQtdPontosX(40);
            objHarmonica.preencherEixoY();

            pontosYHarmonica = objHarmonica.getScores();

            somatorioAmplitude = somatorioAmplitude + objHarmonica.getAmplitudeTensaoHarmonica();
            
            //Sempre que esse método for executado, será adicionado um valor em 
            //  formato de String à série de Fourier
            objDistorcida.calcularSerieFourierAmplitude();
            FormaDeOndaDistorcidaResultante.calcularSerieDeFourierParteHarmonica(objHarmonica);

            return true;            
        }
    }
    
    public boolean acaoButtonEnviarPanelDistHarmonicaUltimo(JSpinner amplitude, JSpinner angulo, JSpinner ordemHarmonica, JPanel panelGraph){
        FormaDeOndaOrdemHarmonica objHarmonica = new FormaDeOndaOrdemHarmonica();
        
        /*   Define os limites dos valores de amplitude e ângulo de fase.
         *   Caso eles não sejam obedecidos, esse método retornará false e 
         * aprecerá uma janela para o usuário descrevendo quais limites foram
         * ultrapassados e qual o intervalo será aceito.
         *   O componente usado para inserir esses valores(JSpinner) não aceita caracteres
         * que não são números. Fazendo desnecessário o uso de excessão nessa 
         * parte do código com esse objetivo.
        */  
        
        if (Double.parseDouble(angulo.getValue().toString()) < -180 || Double.parseDouble(angulo.getValue().toString()) > 180){
            JOptionPane.showMessageDialog(null, "Os ângulos de fase devem estar entre -180° e 180°");
            return false;
        }
        if (Double.parseDouble(amplitude.getValue().toString()) > 220 || Double.parseDouble(amplitude.getValue().toString()) < 0){
            JOptionPane.showMessageDialog(null, "O valor RMS da amplitude deve estar entre 0 e 220");
            return false;
        }
        if (Integer.parseInt(ordemHarmonica.getValue().toString()) < 0 || Integer.parseInt(ordemHarmonica.getValue().toString()) > 15){
            JOptionPane.showMessageDialog(null, "A ordem harmônica deve estar entre 0 e 15");
            return false;
        }
        
        else {        
            objHarmonica.setAnguloDeFaseHarmonica(Double.parseDouble(angulo.getValue().toString()));
            objHarmonica.setAmplitudeTensaoHarmonica(Double.parseDouble(amplitude.getValue().toString()));
            objHarmonica.setOrdemHarmonica(Integer.parseInt(ordemHarmonica.getValue().toString()));

            objHarmonica.setStep(0.001);
            objHarmonica.setQtdPontosX(40);
            objHarmonica.preencherEixoY();

            //  Sempre que esse método for executado, será adicionado um valor em 
            //formato de String à série de Fourier
            pontosYHarmonica = objHarmonica.getScores();
            somatorioAmplitude = somatorioAmplitude + objHarmonica.getAmplitudeTensaoHarmonica();
            FormaDeOndaDistorcidaResultante.calcularSerieDeFourierParteHarmonica(objHarmonica);
            
            return true;
            }
        }

    public void configFormaDeOndaResultante(){
        objDistorcida.setStep(0.001);
        objDistorcida.setQtdPontosX(50);
        objDistorcida.preencherEixoY();
        
        pontosYDistorcida = objDistorcida.getScores();         
    }
    
    public void configPanelResultante(){
        jPanelResultante.getjTextFieldSerieFourierResultante().setText(objDistorcida.getSerieDeFourier());
    }
}
