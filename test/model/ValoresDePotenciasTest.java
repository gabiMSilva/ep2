/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author gabriela
 */
public class ValoresDePotenciasTest {
    
    FormaDeOndaDaCorrente formaDeOndaC;
    FormaDeOndaDaTensao formaDeOndaT;
    ValoresDePotencias potencias;
   
    @Before
    public void testBefore(){
        potencias = new ValoresDePotencias();
        formaDeOndaT = new FormaDeOndaDaTensao();
        formaDeOndaC = new FormaDeOndaDaCorrente();
        
        formaDeOndaC.setAmplitudeCorrente(39.0);
        formaDeOndaC.setAnguloDeFaseCorrente(35.0);
        formaDeOndaT.setAmplitudeTensao(220.0);
        formaDeOndaT.setAnguloDeFaseTensao(0.0);
    }

    @Test
    public void testCalcularPotenciaAtiva() {
        potencias.calcularPotenciaAtiva(formaDeOndaC, formaDeOndaT);
        assertEquals(7028.0, potencias.getValorPotenciaAtiva(), 1.0);        
    }

    @Test
    public void testCalcularPotenciaReativa() {
        potencias.calcularPotenciaReativa(formaDeOndaC, formaDeOndaT);
        assertEquals(-4921.0, potencias.getValorPotenciaReativa(), 1.0);
    }

    @Test
    public void testCalcularPotenciaAparente() {
        potencias.calcularPotenciaAparente(formaDeOndaC, formaDeOndaT);
        assertEquals(8580.0, potencias.getValorPotenciaAparente(), 1.0);
    }

    @Test
    public void testCalcularFatorDePotencia() {
        potencias.calcularFatorDePotencia(formaDeOndaC, formaDeOndaT);
        assertEquals(0.82, potencias.getFatorDePotencia(), 0.01);
    }    
}